<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";

	public function rules()
{
		return[
		[
				'field' =>'kode_barang',
				'label' =>'kode barang',
				'rules' =>'required|max_length[10]',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					'max_length' => 'kode barang tidak boleh lebih dari 10 karakter.',
				]

		],
		[
				'field' =>'nama_barang',
				'label' =>'Nama Barang',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Nama Barang tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'harga_barang',
				'label' =>'Harga Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'Harga barang tidak boleh kosong.',
					'numeric' => 'Harga barang harus angka',
				]
		],
		[
				'field' =>'kode_jenis',
				'label' =>'kode jenis',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode jenis tidak boleh kosong.',


				]

		],
		[
				'field' =>'stok',
				'label' =>'stok',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'stok barang tidak boleh kosong.',
					'numeric' => 'stok barang harus angka',					
				]
		        ]
		];
	}
	
	
	public function tampilDataBarang()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataBarang2()
	{
		$query = $this->db->query("SELECT * FROM jenis_barang as jb inner join barang as br on
		jb.kode_jenis=br.kode_jenis");
		return $query->result();
	
	}
	
	public function tampilDataBarang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_barang)
	{
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_barang)
	{
		//delete from db
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
	
	public function updateStok($kode_barang, $qty)
	{
		$cari_stok = $this->detail($kode_barang);
		foreach ($cari_stok as $data){
			$stok = $data->stok;
		}
		
		$jumlah_stok = $stok + $qty;
		$data_barang['stok'] = $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}

}
