<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";
	
		public function rules()
{
		return[
		[
				'field' =>'kode_supplier',
				'label' =>'kode supplier',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode supplier tidak boleh kosong.',
					'max_length' => 'kode supplier tidak boleh lebih dari 10 karakter.',
				]

		],
		[
				'field' =>'nama_supplier',
				'label' =>'Nama supplier',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Nama supplier tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'alamat',
				'label' =>'alamat',
				'rules' =>'required',
				'errors' =>[
					'required' => 'alamat tidak boleh kosong.',
					
					
				]

		],
		[
				'field' =>'telp',
				'label' =>'telp',
				'rules' =>'required',
				'errors' =>[
					'required' => 'telp tidak boleh kosong.',
					
		
				]
		        ]
		];
	}
	public function tampilDataSupplier()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataSupplier2()
	{
		$query = $this->db->query("SELECT * FROM supplier WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataSupplier3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_supplier)
	{
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['flag']			= 1;
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_supplier)
	{
		//delete from db
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);
	}

}
